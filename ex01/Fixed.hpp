#ifndef FIXED_HPP
# define FIXED_HPP
# include <ostream>

class			Fixed
{
	public:
		~Fixed(void);
		Fixed(void);
		Fixed(const float nbr);
		Fixed(const int nbr);
		Fixed(const Fixed &copy);
		Fixed	&operator = (const Fixed &copy);
		float	&operator << (const Fixed nbr);
		int		getRawBits(void) const;
		void	setRawBits(const int raw);
		float	toFloat(void) const;
		int		toInt(void) const;
	private:
		static const int	fractbits = 8;
		int					value;
};

std::ostream	&operator << (std::ostream &str, Fixed const &fixed_nbr);

#endif
