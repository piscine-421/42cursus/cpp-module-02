#include "Fixed.hpp"
#include <iostream>

Fixed::~Fixed(void)
{
	std::cout << "Destructor called" << std::endl;
}

Fixed::Fixed(const float nbr)
{
	int	fractional;
	int	integer;

	std::cout << "Float constructor called" << std::endl;
	integer = nbr;
	fractional = roundf((nbr - integer) * pow(2, 8));
	value = integer << fractbits;
	value += fractional;
}

Fixed::Fixed(const int nbr)
{
	value = nbr << fractbits;
	std::cout << "Int constructor called" << std::endl;
}

Fixed::Fixed(void)
{
	std::cout << "Default constructor called" << std::endl;
	value = 0;
}

Fixed::Fixed(const Fixed &copy)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = copy;
}

int				Fixed::getRawBits(void) const
{
	return (value);
}

Fixed			&Fixed::operator = (const Fixed &copy)
{
	std::cout << "Copy assignment operator called" << std::endl;
	value = copy.getRawBits();
	return (*this);
}

void			Fixed::setRawBits(const int raw)
{
	value = raw;
}

float			Fixed::toFloat(void) const
{
	return (value * pow(2, -fractbits));
}

int				Fixed::toInt(void) const
{
	return (value >> fractbits);
}

std::ostream	&operator << (std::ostream &str, Fixed const &fixed_nbr)
{
	return (str << fixed_nbr.toFloat());
}
