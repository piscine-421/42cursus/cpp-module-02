#ifndef FIXED_HPP
# define FIXED_HPP
# include <ostream>

class			Fixed
{
	public:
		~Fixed(void);
		Fixed(void);
		Fixed(const float nbr);
		Fixed(const int nbr);
		Fixed(const Fixed &copy);
		int					getRawBits(void) const;
		static Fixed		min(const Fixed &nbr1, const Fixed &nbr2);
		static Fixed		min(Fixed &nbr1, Fixed &nbr2);
		static Fixed		max(const Fixed &nbr1, const Fixed &nbr2);
		static Fixed		max(Fixed &nbr1, Fixed &nbr2);
		Fixed				&operator =(const Fixed &copy);
		bool				operator >(const Fixed &nbr) const;
		bool				operator <(const Fixed &nbr) const;
		bool				operator >=(const Fixed &nbr) const;
		bool				operator <=(const Fixed &nbr) const;
		bool				operator ==(const Fixed &nbr) const;
		bool				operator !=(const Fixed &nbr) const;
		Fixed				operator +(const Fixed &nbr) const;
		Fixed				operator -(const Fixed &nbr) const;
		Fixed				operator *(const Fixed &nbr) const;
		Fixed				operator /(const Fixed &nbr) const;
		Fixed				&operator ++(void);
		Fixed				operator ++(int dummy);
		Fixed				&operator --(void);
		Fixed				operator --(int dummy);
		void				setRawBits(const int raw);
		float				toFloat(void) const;
		int					toInt(void) const;
	private:
		static const int	fractbits = 8;
		int					value;
};

std::ostream	&operator <<(std::ostream &str, Fixed const &fixed_nbr);

#endif
