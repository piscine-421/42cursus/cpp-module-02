#include "Fixed.hpp"
#include <iostream>

int	main(int argc, char **argv)
{
	if (argc < 3)
	{
		std::cout << "Usage:\n\tArg1: first operand\n\tArg2: second operand";
		return (EXIT_FAILURE);
	}
	std::string	arg1 = argv[1];
	std::string	arg2 = argv[2];
	Fixed		num1(std::stof(arg1));
	Fixed		num2(std::stof(arg2));
	if (num1 > num2)
		std::cout << num1 << " > " << num2 << " is true" << std::endl;
	else
		std::cout << num1 << " > " << num2 << " is false" << std::endl;
	if (num1 < num2)
		std::cout << num1 << " < " << num2 << " is true" << std::endl;
	else
		std::cout << num1 << " < " << num2 << " is false" << std::endl;
	if (num1 >= num2)
		std::cout << num1 << " >= " << num2 << " is true" << std::endl;
	else
		std::cout << num1 << " >= " << num2 << " is false" << std::endl;
	if (num1 <= num2)
		std::cout << num1 << " <= " << num2 << " is true" << std::endl;
	else
		std::cout << num1 << " <= " << num2 << " is false" << std::endl;
	if (num1 == num2)
		std::cout << num1 << " == " << num2 << " is true" << std::endl;
	else
		std::cout << num1 << " == " << num2 << " is false" << std::endl;
	if (num1 != num2)
		std::cout << num1 << " != " << num2 << " is true" << std::endl;
	else
		std::cout << num1 << " != " << num2 << " is false" << std::endl;
	std::cout << num1 << " + " << num2 << " is " << num1 + num2 << std::endl;
	std::cout << num1 << " - " << num2 << " is " << num1 - num2 << std::endl;
	std::cout << num1 << " * " << num2 << " is " << num1 * num2 << std::endl;
	std::cout << num1 << " / " << num2 << " is " << num1 / num2 << std::endl;
	std::cout << "num1++ is " << num1++ << std::endl;
	std::cout << "num1 is " << num1 << std::endl;
	std::cout << "num1-- is " << num1-- << std::endl;
	std::cout << "num1 is " << num1 << std::endl;
	std::cout << "++" << num1 << " is " << ++num1 << std::endl;
	std::cout << "--" << num1 << " is " << --num1 << std::endl;
	return (0);
}
