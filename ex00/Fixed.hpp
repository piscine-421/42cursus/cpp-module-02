#ifndef FIXED_HPP
# define FIXED_HPP

class	Fixed
{
	public:
		~Fixed(void);
		Fixed(void);
		Fixed(const Fixed &copy);
		Fixed	&operator = (const Fixed &copy);
		int		getRawBits(void) const;
		void	setRawBits(const int raw);
	private:
		static const int	fractbits = 8;
		int					value;
};

#endif
