#include "Fixed.hpp"
#include <iostream>

Fixed::~Fixed(void)
{
}

Fixed::Fixed(const float nbr)
{
	int	fractional;
	int	integer;

	integer = nbr;
	fractional = roundf((nbr - integer) * pow(2, 8));
	value = integer << fractbits;
	value += fractional;
}

Fixed::Fixed(const int nbr)
{
	value = nbr << fractbits;
}

Fixed::Fixed(void)
{
	value = 0;
}

Fixed::Fixed(const Fixed &copy)
{
	*this = copy;
}

int				Fixed::getRawBits(void) const
{
	return (value);
}

Fixed			Fixed::max(const Fixed &nbr1, const Fixed &nbr2)
{
	if (nbr1 > nbr2)
		return (nbr1);
	else
		return (nbr2);
}

Fixed			Fixed::max(Fixed &nbr1, Fixed &nbr2)
{
	if (nbr1 > nbr2)
		return (nbr1);
	else
		return (nbr2);
}

Fixed			Fixed::min(const Fixed &nbr1, const Fixed &nbr2)
{
	if (nbr1 < nbr2)
		return (nbr1);
	else
		return (nbr2);
}

Fixed			Fixed::min(Fixed &nbr1, Fixed &nbr2)
{
	if (nbr1 < nbr2)
		return (nbr1);
	else
		return (nbr2);
}

Fixed			&Fixed::operator =(const Fixed &copy)
{
	value = copy.getRawBits();
	return (*this);
}

bool			Fixed::operator >(const Fixed &nbr) const
{
	bool	a = toFloat() > nbr.toFloat();

	return (a);
}

bool			Fixed::operator <(const Fixed &nbr) const
{
	bool	a = toFloat() < nbr.toFloat();

	return (a);
}

bool			Fixed::operator >=(const Fixed &nbr) const
{
	bool	a = toFloat() >= nbr.toFloat();

	return (a);
}

bool			Fixed::operator <=(const Fixed &nbr) const
{
	bool	a = toFloat() <= nbr.toFloat();

	return (a);
}

bool			Fixed::operator ==(const Fixed &nbr) const
{
	bool	a = toFloat() == nbr.toFloat();

	return (a);
}

bool			Fixed::operator !=(const Fixed &nbr) const
{
	bool	a = toFloat() != nbr.toFloat();

	return (a);
}

Fixed			Fixed::operator +(const Fixed &nbr) const
{
	Fixed	a(toFloat() + nbr.toFloat());

	return (a);
}

Fixed			Fixed::operator -(const Fixed &nbr) const
{
	Fixed	a(toFloat() - nbr.toFloat());

	return (a);
}

Fixed			Fixed::operator *(const Fixed &nbr) const
{
	Fixed	a(toFloat() * nbr.toFloat());

	return (a);
}

Fixed			Fixed::operator /(const Fixed &nbr) const
{
	Fixed	a(toFloat() / nbr.toFloat());

	return (a);
}

Fixed			&Fixed::operator ++(void)
{
	value++;

	return (*this);
}

Fixed			Fixed::operator ++(int dummy)
{
	Fixed	old(*this);

	(void)dummy;
	value++;
	return (old);
}

Fixed			&Fixed::operator --(void)
{
	value--;

	return (*this);
}

Fixed			Fixed::operator --(int dummy)
{
	Fixed	old(*this);

	(void)dummy;
	value--;
	return (old);
}

void			Fixed::setRawBits(const int raw)
{
	value = raw;
}

float			Fixed::toFloat(void) const
{
	return (value * pow(2, -fractbits));
}

int				Fixed::toInt(void) const
{
	return (value >> fractbits);
}

std::ostream	&operator <<(std::ostream &str, Fixed const &fixed_nbr)
{
	return (str << fixed_nbr.toFloat());
}
