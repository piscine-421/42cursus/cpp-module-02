#include <iostream>
#include "Point.hpp"

int	main(void)
{
	Point	p1(0, 0);
	Point	p2(0, 20);
	Point	p3(20, 10);
	Point	p4(20, 11);

	if (bsp(p1, p2, p3, p4))
		std::cout << "Point is in triangle!" << std::endl;
	else
		std::cout << "Point not in triangle!" << std::endl;
	return (0);
}
