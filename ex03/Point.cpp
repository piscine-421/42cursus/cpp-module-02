#include "Point.hpp"

Fixed	Point::get_x(void) const
{
	return (x);
}

Fixed	Point::get_y(void) const
{
	return (y);
}

Point	&Point::operator =(const Point &copy)
{
	static Point	new_point(copy);

	return (new_point);
}

Point::~Point(void)
{
}

Point::Point(void): x(0), y(0)
{
}

Point::Point(const float new_x, const float new_y): x(new_x), y(new_y)
{
}

Point::Point(const Point &copy): x(copy.get_x()), y(copy.get_y())
{
}
