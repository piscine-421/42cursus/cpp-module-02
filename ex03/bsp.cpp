#include "Fixed.hpp"
#include "Point.hpp"

static Fixed	get_m(const Point p1, const Point p2)
{
	Fixed	x1;
	Fixed	x2;
	Fixed	y1;
	Fixed	y2;

	if (p1.get_x() <= p2.get_x())
	{
		x1 = p1.get_x();
		x2 = p2.get_x();
		y1 = p1.get_y();
		y2 = p2.get_y();
	}
	else
	{
		x1 = p2.get_x();
		x2 = p1.get_x();
		y1 = p2.get_y();
		y2 = p1.get_y();
	}
	return ((y2 - y1) / (x2 - x1));
}

static Fixed	get_h(const Point p, const Fixed m)
{
	return (p.get_y() - m * p.get_x());
}

static Fixed	get_y(const Point p1, const Point p2, const Point p3)
{
	Fixed	h;
	Fixed	m;

	m = get_m(p1, p2);
	h = get_h(p1, m);
	return (m * p3.get_x() + h);
}

bool			bsp(const Point a, const Point b, const Point c, const Point point)
{
	if (get_y(a, b, c) < c.get_y() && get_y(a, b, point) > point.get_y())
		return (false);
	if (get_y(a, b, c) > c.get_y() && get_y(a, b, point) < point.get_y())
		return (false);
	if (get_y(a, c, b) < b.get_y() && get_y(a, c, point) > point.get_y())
		return (false);
	if (get_y(a, c, b) > b.get_y() && get_y(a, c, point) < point.get_y())
		return (false);
	if (get_y(b, c, a) < a.get_y() && get_y(b, c, point) > point.get_y())
		return (false);
	if (get_y(b, c, a) > a.get_y() && get_y(b, c, point) < point.get_y())
		return (false);
	return (true);
}
