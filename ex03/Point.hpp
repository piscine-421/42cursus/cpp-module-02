#ifndef POINT_HPP
# define POINT_HPP
# include "Fixed.hpp"

class	Point
{
	public:
		Fixed		get_x(void) const;
		Fixed		get_y(void) const;
		Point		&operator =(const Point &copy);
		~Point(void);
		Point(void);
		Point(const float new_x, const float new_y);
		Point(const Point &copy);
	private:
		const Fixed	x;
		const Fixed	y;
};

bool	bsp(const Point a, const Point b, const Point c, const Point point);

#endif
